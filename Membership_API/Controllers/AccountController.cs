﻿using Membership_API.Models;
using Membership_API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Membership_API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class AccountController : ControllerBase
    {
        private readonly AccountService _accountsService;

        public AccountController(AccountService accountService) =>
            _accountsService = accountService;

        [HttpGet]
        public async Task<List<Account>> Get() =>
            await _accountsService.GetAsync();

        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<Account>> Get(string id)
        {
            var accountGet = await _accountsService.GetAsync(id);

            if (accountGet is null)
            {
                return NotFound();
            }

            return accountGet;
        }

        [HttpPost]
        public async Task<IActionResult> Post(Account newAcccount)
        {
            await _accountsService.CreateAsync(newAcccount);

            return CreatedAtAction(nameof(Get), new { id = newAcccount.Id }, newAcccount);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update(string id, Account updatedAcccount)
        {
            var accountGet = await _accountsService.GetAsync(id);

            if (accountGet is null)
            {
                return NotFound();
            }

            updatedAcccount.Id = accountGet.Id;

            await _accountsService.UpdateAsync(id, updatedAcccount);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string id)
        {
            var accountGet = await _accountsService.GetAsync(id);

            if (accountGet is null)
            {
                return NotFound();
            }

            await _accountsService.RemoveAsync(accountGet.Id);

            return NoContent();
        }

    }
}
