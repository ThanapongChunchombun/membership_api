﻿using Membership_API.Models;
using Microsoft.AspNetCore.Mvc;
using Membership_API.Services;
using MongoDB.Bson.IO;
using System.Text.Json;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.Security.Cryptography;
using Membership_API.Helper;
using BCrypt.Net;

namespace Membership_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AuthenController : ControllerBase
    {
        private readonly AuthenService _authenService;
        private readonly JWTSettings _jwtsettings;

        public AuthenController(AuthenService authenService) =>
            _authenService = authenService;

        [HttpGet]
        public async Task<List<AuthenToken>> Get() =>
         await _authenService.GetAsync();


        [HttpPost("RegisterToken")]
        public async Task<ActionResult> RegisterToken(AuthenToken newAcccount)
        {
            try
            {
                //newAcccount.Password = PasswordHash.HashPassword(newAcccount.Password);
                await _authenService.CreateAsync(newAcccount);

                var user = await _authenService.GetAsync(newAcccount.Username, newAcccount.Password);

                UserWithToken userWithToken = null;

                if (user != null)
                {


                    RefreshToken refreshToken = GenerateRefreshToken();
                    user.RefreshTokens.Add(refreshToken);
                    await _authenService.UpdateAsync(user.Id, user);

                    userWithToken = new UserWithToken(user);
                    userWithToken.RefreshToken = refreshToken.Token;
                    userWithToken.AccessToken = GenerateAccessToken(user.Id);

                }

                if (userWithToken == null)
                {
                    return NotFound("NotFound");
                }

                return Ok(userWithToken);
            } catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

        }

        [HttpPost("AuthenticationToken")]
        public async Task<ActionResult> AuthenticationToken([FromBody] Login newAuthen)
        {

            try
            {
                var password = await _authenService.GetAsync(newAuthen.Username);
                var validPassword = BCrypt.Net.BCrypt.Verify(newAuthen.Password, password.Password);
                //return Ok(validPassword);
                if (validPassword)
                {
                    var user = await _authenService.GetAsync(newAuthen.Username, password.Password);

                    UserWithToken userWithToken = null;

                    if (user != null)
                    {
                        RefreshToken refreshToken = GenerateRefreshToken();
                        user.RefreshTokens.Add(refreshToken);
                        await _authenService.UpdateAsync(user.Id, user);

                        userWithToken = new UserWithToken(user);
                        userWithToken.RefreshToken = refreshToken.Token;
                        userWithToken.AccessToken = GenerateAccessToken(user.Username);
                    }

                    if (user == null)
                    {
                        return NotFound();
                    }


                    return Ok(userWithToken);
                }
                else
                {
                    return Unauthorized();
                }

            } catch (Exception ex)
                   {
                      return BadRequest(ex.Message);
                   }


        }

        [HttpPost("Authentication")]
        public async Task<ActionResult> Authentication(Login newAuthen)
        {
            var accountGet = await _authenService.GetAsync(newAuthen.Username, newAuthen.Password);

            if (accountGet == null)
            {
                return NotFound();
            }

            return Ok(new
            {
                Username = accountGet.Username,
                Name = accountGet.Name,
                Token = "fake-jwt-token"
            });
        }
        private string GenerateAccessToken(string Username)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes(_jwtsettings.SecretKey);
            var key = Encoding.ASCII.GetBytes("T5MDdVyrm8AmotzNv8rfWnof");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                //Subject = new ClaimsIdentity(new Claim[]
                //{
                //    new Claim(ClaimTypes.Name, userId)
                //}),
                Subject = new ClaimsIdentity(new[] { new Claim("id", Username) }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        private RefreshToken GenerateRefreshToken()
        {
            RefreshToken refreshToken = new RefreshToken();

            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                refreshToken.Token = Convert.ToBase64String(randomNumber);
            }
            refreshToken.ExpiryDate = DateTime.UtcNow.AddMonths(6);

            return refreshToken;
        }



    }
}
