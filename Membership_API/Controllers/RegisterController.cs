﻿using Membership_API.Models;
using Membership_API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Membership_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegisterController : ControllerBase
    {
        private readonly RegisterService _registerService;

        public RegisterController(RegisterService registerService) =>
            _registerService = registerService;

        [HttpGet]
        public async Task<List<Register>> Get() =>
            await _registerService.GetAsync();


        [HttpPost]
        public async Task<IActionResult> Post(Register newAcccount)
        {
            await _registerService.CreateAsync(newAcccount);

            return CreatedAtAction(nameof(Get), new { id = newAcccount.Id }, newAcccount);
        }
    }
}
