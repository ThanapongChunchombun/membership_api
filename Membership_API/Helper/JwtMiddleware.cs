﻿using Membership_API.Models;
using Membership_API.Services;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.Json;

namespace Membership_API.Helper
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        //private readonly AppSettings _appSettings;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
            //_appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context, AuthenService authenService)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                attachUserToContext(context, authenService, token);

            await _next(context);
        }

        private async void attachUserToContext(HttpContext context, AuthenService authenService, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("T5MDdVyrm8AmotzNv8rfWnof");
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                //Console.WriteLine(jwtToken);
                //var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
                var userName = jwtToken.Claims.First(x => x.Type == "id").Value;
                //Console.WriteLine(userName);

                // attach user to context on successful jwt validation
                var user = await authenService.GetAsync(userName);
                if (user != null)
                {
                    var user2 = JsonSerializer.Serialize(user);
                    context.Items["User"] = user2;
                }
                //var user = await authenService.GetUserIdAsync(userId);
                Console.WriteLine(user);
                //context.Items["User"] = await authenService.GetAsync(userName);
                //await _next(context);


            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}
