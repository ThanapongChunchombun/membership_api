﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Membership_API.Models
{
    public class Authen
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        public string Name { get; set; } = null!;
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;

    }
}
