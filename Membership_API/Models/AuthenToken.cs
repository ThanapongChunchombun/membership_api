﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Membership_API.Models
{
    public class AuthenToken
    {
        public AuthenToken()
        {
            RefreshTokens = new HashSet<RefreshToken>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        public string Name { get; set; } = null!;
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
