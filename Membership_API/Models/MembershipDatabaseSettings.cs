﻿namespace Membership_API.Models
{
    public class MembershipDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

        public string EmployeeCollectionName { get; set; } = null!;

        public string AccountCollectionName { get; set; } = null!;

    }
}
