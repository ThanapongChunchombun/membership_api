﻿namespace Membership_API.Models
{
    public partial class Role
    {
        public Role()
        {
            Users = new HashSet<Authen>();
        }

        public short RoleId { get; set; }
        public string RoleDesc { get; set; }

        public virtual ICollection<Authen> Users { get; set; }
    }
}
