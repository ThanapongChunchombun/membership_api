﻿namespace Membership_API.Models
{
    public class UserWithToken : AuthenToken
    {

        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public UserWithToken(AuthenToken user)
        {
            this.Id = user.Id;
            this.Name = user.Name;
            this.Username = user.Username;
        }
    }
}
