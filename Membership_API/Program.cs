using Membership_API.Helper;
using Membership_API.Models;
using Membership_API.Services;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

// Add services to the container.
builder.Services.Configure<MembershipDatabaseSettings>(
    builder.Configuration.GetSection("MembershipDatabase"));
builder.Services.AddSingleton<AccountService>()
                 .AddSingleton<RegisterService>()
                 .AddSingleton<AuthenService>();
//builder.Services.AddAuthentication(option =>
//{
//    option.DefaultAuthenticateScheme = "JwtBearer";
//    option.DefaultChallengeScheme = "JwtBearer";
//})
//    .AddJwtBearer("JwtBearer", jwtBearerOptions =>
//    {
//        jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
//        {
//            ValidateIssuerSigningKey = true,
//            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("T5MDdVyrm8AmotzNv8rfWnof")),
//            ValidateIssuer = false,
//            ValidateAudience = false,
//            ValidateLifetime = true,
//            ClockSkew = TimeSpan.FromMinutes(5)
//        };

//    });

builder.Services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = "JwtBearer";
    option.DefaultChallengeScheme = "JwtBearer";
})
             //�ç�����µç���
            .AddJwtBearer("JwtBearer", x =>
            {
                //x.RequireHttpsMetadata = true;
                //x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("T5MDdVyrm8AmotzNv8rfWnof")),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });


//to validate the token which has been sent by clients
//var appSettings = jwtSection.Get<JWTSettings>();
//var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);


builder.Services.AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddCors(options =>
{ 
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      builder =>
                      {
                          builder.WithOrigins("https://localhost:7258/", 
                                              "https://localhost:7258")
                                                                      .AllowAnyHeader()
                                                                      .AllowAnyOrigin()
                                                                      .AllowAnyMethod();
                      });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseHttpsRedirection();

app.UseMiddleware<JwtMiddleware>();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.UseCors(MyAllowSpecificOrigins);

app.Run();
