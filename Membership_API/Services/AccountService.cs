﻿using Membership_API.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Membership_API.Services
{
    public class AccountService
    {
        readonly IMongoCollection<Account> _AccountCollection;

        public AccountService(
            IOptions<MembershipDatabaseSettings> membershipDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                membershipDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                membershipDatabaseSettings.Value.DatabaseName);

            _AccountCollection = mongoDatabase.GetCollection<Account>(
                membershipDatabaseSettings.Value.EmployeeCollectionName);
        }

        public async Task<List<Account>> GetAsync() =>
            await _AccountCollection.Find(_ => true).ToListAsync();

        public async Task<Account?> GetAsync(string id) =>
            await _AccountCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task CreateAsync(Account newAcccount) =>
            await _AccountCollection.InsertOneAsync(newAcccount);

        public async Task UpdateAsync(string id, Account updatedAcccount) =>
            await _AccountCollection.ReplaceOneAsync(x => x.Id == id, updatedAcccount);

        public async Task RemoveAsync(string id) =>
            await _AccountCollection.DeleteOneAsync(x => x.Id == id);

    }
}
