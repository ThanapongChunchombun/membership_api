﻿using Membership_API.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Membership_API.Services
{
    //public class AuthenService
    //{
    //    readonly IMongoCollection<Authen> _AuthenCollection;

    //    public AuthenService(
    //        IOptions<MembershipDatabaseSettings> membershipDatabaseSettings)
    //    {
    //        var mongoClient = new MongoClient(
    //            membershipDatabaseSettings.Value.ConnectionString);

    //        var mongoDatabase = mongoClient.GetDatabase(
    //            membershipDatabaseSettings.Value.DatabaseName);

    //        _AuthenCollection = mongoDatabase.GetCollection<Authen>(
    //            membershipDatabaseSettings.Value.AccountCollectionName);
    //    }

    //    public async Task<List<Authen>> GetAsync() =>
    //      await _AuthenCollection.Find(_ => true).ToListAsync();

    //    public async Task<Authen?> GetAsync(string username, string password) =>
    //      await _AuthenCollection.Find(x => x.Username == username && x.Password == password ).FirstOrDefaultAsync();

    //    public async Task CreateAsync(Authen newAcccount) =>
    //      await _AuthenCollection.InsertOneAsync(newAcccount);

    //}

    public class AuthenService
    {
        readonly IMongoCollection<AuthenToken> _AuthenCollection;


        public AuthenService(
            IOptions<MembershipDatabaseSettings> membershipDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                membershipDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                membershipDatabaseSettings.Value.DatabaseName);

            _AuthenCollection = mongoDatabase.GetCollection<AuthenToken>(
                membershipDatabaseSettings.Value.AccountCollectionName);
        }

        public async Task<List<AuthenToken>> GetAsync() =>
          await _AuthenCollection.Find(_ => true).ToListAsync();
        public async Task<AuthenToken?> GetAsync(string username) =>
          await _AuthenCollection.Find(x => x.Username == username).FirstOrDefaultAsync();

        public async Task<AuthenToken?> GetAsync(string username, string password) =>
          await _AuthenCollection.Find(x => x.Username == username && x.Password == password).FirstOrDefaultAsync();
        //public async Task<AuthenToken?> GetUserIdAsync(string UserId) =>
        //  await _AuthenCollection.Find(x => x.Id == UserId).FirstOrDefaultAsync();
         public async Task<AuthenToken?> GetUserIdAsync(string UserId) =>
          await _AuthenCollection.Find(x => x.Id == UserId).FirstOrDefaultAsync();

        public async Task CreateAsync(AuthenToken newAcccount) =>
          await _AuthenCollection.InsertOneAsync(newAcccount);

        public async Task UpdateAsync(string id, AuthenToken updatedAcccount) =>
          await _AuthenCollection.ReplaceOneAsync(x => x.Id == id, updatedAcccount);

    }
}
