﻿using Membership_API.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Membership_API.Services
{ 
    public class RegisterService
    {
        readonly IMongoCollection<Register> _RegisterCollection;

        public RegisterService(
            IOptions<MembershipDatabaseSettings> membershipDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                membershipDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                membershipDatabaseSettings.Value.DatabaseName);

            _RegisterCollection = mongoDatabase.GetCollection<Register>(
                membershipDatabaseSettings.Value.AccountCollectionName);
        }

        public async Task<List<Register>> GetAsync() =>
          await _RegisterCollection.Find(_ => true).ToListAsync();

        public async Task CreateAsync(Register newAcccount) =>
          await _RegisterCollection.InsertOneAsync(newAcccount);

    }
}
